#!/bin/python

import cgi, cgitb
cgitb.enable()
form=cgi.FieldStorage()

firstname = form.getvalue('firstName')
surname=form.getvalue('surName')
uname=form.getvalue('nickName')
dob=form.getvalue('dob')
email=form.getvalue('email')
gender=form.getvalue('gender')

#     fName,  sName,      uName,       dob,    gender
info=[firstname, surname, email, uname, dob, gender]
pw=form.getvalue("pw1")
print ("Content-Type: text/html\n")

print("""<!DOCTYPE html>
<html>
	<head>
		<title>Exxcelius</title>
		<link rel="stylesheet" type="text/css" href="../main.css">
		<link rel="stylesheet" type="text/css" href="../register.css">
	</head>

	<body>
		<div class="row">
			<div class="head" id="header">
				<img id="headBG" src="SPACEHOLDER" alt="Logo">
				<div id="headOPT">
					<button></button>
				</div>
				<div id="AdsHead">
					Please deactivate your Adblocker<br>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col-1" id="columnLeft">
				Your account<br>
				Your posts<br>
				Contact<br>
				Imprint<br>
			</div>
			<div class="col-10" id="main">
				<div id="infoBox">
					<div id="tags">
						<p>
							FirstName:<br>
							Surname:<br>
							Username:<br>
							Date of Birth:<br>
							Gender:<br>
						</p>
					</div>
					<div id="info">
						<p>""")
for f in info:
	print( "							" + str(f) + "<br>")

print("""					</p>
					</div>
				</div>
				<div>
					<button id="back" onclick="">Go Back</button>
					<button id="continue" onclick="">Continue</button>
				</div>
			</div>
			<div class="col-1" id="columnright">
			</div>
		</div>
	</body>
</html>
""")
