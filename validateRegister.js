function validateForm(){
	console.log("validateForm() called!");
	var pw1, pw2;
	pw1 = document.forms.regForm["pw1"].value;
	pw2 = document.forms.regForm["pw2"].value;
	console.log(pw1 == pw2);
					
	if (pw1 != pw2){
		console.log("Path:passwords differ");
		document.forms.regForm["pw1"].value = "";
		document.forms.regForm["pw2"].value = "";
		document.getElementById("pwWarning").innerHTML = "Passwords must be equal";
		return false;
	} else{
		console.log("Path:passwords same");
		document.getElementById("pwWarning").innerHTML = "";
		return true;
	}
}
